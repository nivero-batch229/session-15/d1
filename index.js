// JS Comments (ctrl+/) - Acts as a guide for the programmer
// Comments are disregarded no matter how long it may be.
	// Mulit-line Comments (ctrl+shift+/)


// Statements - programming instruction that being told to computer to be performed. Usually ends with semicolon (;)
// Information


// Syntax - set of rules that describes how statements must be constructed.


// alert("Hello Again!");

// Statement with a correct syntax
console.log("Hello World!");

// JS is a lose type programming language - it will still accept the inputs even with spaces in between or no semicolon at the end of each syntax.
console. log (  "  Hello World!");
console.
log
(
"Welcome Dude"
)

// [Section] Variables - is used as a container or storage.
// Declaring variables - tells our device that a variable name is created and ready to store data.

// Syntax - let/const variableName;
// e.g below:

// "let" - is a keyword that is usually used to declare a variables
// OOP - Objext Oriented Programming
// Variables must be declared first before calling console
// Variable name should start with a lowercase letter and should be comprehensive or descriptive
// for constant variable - use const keyword
// do not use spaces on the declared variable
// do not use a variable name more than once

let myVariable;
let hello;
console.log(myVariable);
console.log(hello)

// equal sign = is being used to give value to a variable
// Different Casing Style
// cammel casing - nickName
// snake case - nick_name
// kebab case - nick-name
let nickName = "Mai";
let nick_name = "Mai";
console.log(nickName);
console.log(nick_name);

// bad variable name
let pokemon = 2500;

// declaring and initializing variables
// syntax is lt/const variableName = value
let productName = "guitar cords";
console.log(productName);

let productPrice = 1050;
console.log(productPrice);

// reassigning a variable to a variable
productPrice = 1112;
console.log(productPrice);

let neighbor = "marites";
neighbor = "karen";
console.log(neighbor);

let supplier;
supplier = "John Lazy";
console.log(supplier);

// let versus const
// let V values can be changed and reassign new values
// const V values cannot be changed

// use const for variables that cannot be changed in general and has an absolute value like hours in a day and pie value
const hoursInAday = 24;
console.log(hoursInAday);

const pie = 3.14;
console.log(pie);

// local/global scope variable
// variables that are inside brackets must be called by console inside the same brackets
// outer variable can be called inside the bracket
let outerVariable = "Yow"; // global variable
{
	let innerVariable = "YesWorld"; //local variable
	console.log(innerVariable);
	console.log(outerVariable);
}
console.log(outerVariable);

/*
multiple variable declaration - may be declared in one line. It is convenient and easier to read the code.

let productCode = "pc107";
let productBrand = "asus";
*/

let productCode = "pc017"; 
productBrand = "asus";
console.log(productCode, productBrand);

/*
variable let is a reserved keyword and will cause error when used as variable name
const let = "hello"
console.log(let);
*/

// Data Types
// Strings are series of chareacters that creates a word

let country = "Philippines";
let province = "Bicol";

// Concatenating string - is combining string and uses the + symbol
let fullAddress = province + " , " + country
console.log(fullAddress);

let greeting = " I live in the " + country;
console.log(greeting);

// Escape character (\)
// "\n" referes to creating a new line between text
let mailAddress = "Rodriguez Rizal\n\nPhilippines"
console.log(mailAddress);

// when using single quotations marks, always use sing backslash before the word with apostrophe
let hungry = "Papa John's Pizza";
console.log(hungry);
mesasge = 'PapaJohn\'s Pizza';

// Numbers - integers or whole numbers
let salary = 50000;
console.log(salary);

// Decimal or fractions
let grade = 98.9;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining Text and Strings
console.log("John's grade last quarter is " + grade);

// 2 bollean values
// 1. true
// 2. false
let isMarried = false;
let isGoodconduct = true;
console.log("isMarried: " + isMarried)
console.log("isGoodconduct: " + isGoodconduct);

// Arrays [] - special kind of data type that is use to store multiple value with the same data types
// syntax let/const arrayName = [element A, elmentB, . . ]
let grades = [98.9, 92.1, 90.2];
console.log(grades);

let details = ["John", "Smith", 32, true];
console.log(details);

// Objects - holds properties that describe the variable
// syntax: let/const objectName = {propertyA: value, propertyB: value, . . .}
// No need to use semicolon to end the syntax
let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	contact: ["6391111", "789456123"],
	address: {
		houseNumber: "123",
		city: "Manila",
	}
}
console.log(person);

let myGrades = {
	firstGrading: 98.7,
	sedondGrading: 92.1,
	thirdGrading: 90.2,
	fourthGrading: 94.6
}
console.log(myGrades);

// checking of data type
console.log(typeof grades);
console.log(typeof person);
console.log(typeof isMarried);

// in programming, we always start counting by 0
const anime = ["one piece", "one punch man", "atatck on titan"]
anime[0] = "kimetsu no yaiba";
console.log(anime);

// Null versus Undefined
// null is when variable is 0 or empty value
// undefined is when a variable has no vlaue upon declaration
let spouse = null; //null or zero or empty

